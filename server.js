require('dotenv').config();

const express = require('express'),
  app = express(),
  { createClient } = require('@supabase/supabase-js'),
  http = require('http'),
  server = http.createServer(app),
  socketio = require('socket.io'),
  io = socketio(server),
  countryReverseGeocoding = require('country-reverse-geocoding').country_reverse_geocoding();

const supabase = createClient(process.env.SUPABASE_URL, process.env.SUPABASE_KEY);
const downloadsRoutes = require('./routes/downloads');

// Middlewares
app.use(express.json());
app.use(express.static(process.cwd() + `${process.env.ANGULAR_STATIC_FOLDER_PATH}`));
app.use((req, res, next) => {
  req.io = io;
  req.supabase = supabase;
  req.geo = countryReverseGeocoding;
  next();
});

io.on('connection', (socket) => {
  // Listen to changes
  const downloadsSubscription = supabase
    .from('downloads')
    .on('INSERT', download => {
      io.sockets.setMaxListeners(0);
      const country = countryReverseGeocoding.get_country(download.new.latitude, download.new.longitude);
      download.new.code = country?.code;
      download.new.name = country?.name;
      socket.emit('downloadReceived', download);
    }).subscribe();
});

app.get('/', (req, res) => {
  res.sendFile(process.cwd() + `${process.env.ANGULAR_STATIC_FOLDER_PATH}/index.html`)
});

// Requiring routes
app.use("/downloads", downloadsRoutes);

app.get('*', async (req, res) => {
  return res.redirect('/');
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => console.info(`Server running on port ${PORT}!`));
