require('dotenv').config();

const express = require('express'),
  router = express.Router();

/*
 Get downloads
*/
router.get("/", async (req, res) => {
  try {
    const downloads = await req.supabase
      .from('downloads')
      .select('*');

    const results = prepareResults(req, downloads.body);

    return res.json(results);
  } catch (error) {
    console.log(error);
  }
});

/*
Create a download
*/
router.post("/", async (req, res) => {
  try {
    const { latitude, longitude } = req.body;

    const newDownload = await req.supabase
      .from('downloads')
      .insert([
        {
          latitude,
          longitude,
        }
      ])

    return res.json(newDownload.body);
  } catch (error) {
    console.log(error);
  }
});

/*
 Get downloads by time
*/
router.get("/:time", async (req, res) => {
  try {
    const timeParam = req.params.time;

    const period = {
      morning: 'morning',
      afternoon: 'afternoon',
      evening: 'evening',
    }

    if (!period[timeParam]) {
      return res.redirect('/');
    }

    const downloads = await req.supabase
      .rpc(`${timeParam}_downloads`);

    const results = prepareResults(req, downloads.body);
    return res.json(results);
  } catch (error) {
    console.log(error);
    return res.json([]);
  }
});

const prepareResults = (req, downloads) => {
  return downloads?.map((download) => {
    const country = req.geo.get_country(download.latitude, download.longitude);
    download.code = country?.code;
    download.name = country?.name;
    return download;
  });
}

module.exports = router;
