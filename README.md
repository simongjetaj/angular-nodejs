# Stats

Few words about the project:
It is built using Angular and Node.js as two main technologies for the front-end and back-end.
I tried to explore these two technologies for the first time together.
Another new thing was the use of Supabase, it is a product that I found a few weeks ago (one of the top marketplace products on Digital Ocean). https://supabase.io/

Note: I would have liked this application to be containerized also, but I ran out of time.

### Run

Assuming, You have _Node_ and _npm_ properly installed on your machine, you need to:

1. Download/clone the project

2. Navigate to inside the project folder on a terminal, and execute `npm i`.

3. Execute `npm start` command and everything should be running on _localhost:3000_

## Test
To create a download in a specific location, you can try to use the below command.
You will see in the map the number of downloads being increased real-time.

```
curl --request POST \
  --url http://localhost:3000/downloads \
  --header 'content-type: application/json' \
  --data '{
	 "latitude": "41.3275",
   "longitude": "19.8187"
}'
```
