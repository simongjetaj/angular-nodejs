all: docker-build

docker-build:
	docker-compose down -v --remove-orphans
	docker-compose build
	docker-compose up -d
	sleep 5s
	@echo Project is successfully built!
	@echo Please, type localhost:3000 into your favourite web browser.
