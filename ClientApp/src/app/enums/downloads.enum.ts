export enum DownloadsTimePeriodEnum {
  Morning = 'morning',
  Afternoon = 'afternoon',
  Evening = 'evening',
}
