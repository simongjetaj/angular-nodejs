import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IDownload } from '../models/downloads.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DownloadsService {
  constructor(@Inject(HttpClient) private httpClient: HttpClient) {}

  getDownloads(timePeriod?: string): Observable<IDownload[]> {
    let url = `${environment.downloadsApi}/downloads`;

    if (timePeriod) {
      url += `/${timePeriod}`;
    }

    return this.httpClient.get<IDownload[]>(url);
  }
}
