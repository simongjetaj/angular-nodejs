export interface IDownload {
  id: number;
  latitude: number;
  longitude: number;
  downloaded_at: Date;
  code?: string;
  name?: string;
}
