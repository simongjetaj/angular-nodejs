require('dotenv').config();

const { src, dest, series, parallel } = require('gulp');
const del = require('del');
const fs = require('fs');
const zip = require('gulp-zip');
const log = require('fancy-log');
const webpack_stream = require('webpack-stream');
const webpack_config = require('./webpack.config.js');
var exec = require('child_process').exec;

const paths = {
  prod_build: 'prod-build',
  server_file_name: 'server.bundle.js',
  angular_src: `${process.env.ANGULAR_FOLDER_NAME}/dist/**/*`,
  angular_dist: `prod-build/${process.env.ANGULAR_FOLDER_NAME}/dist`,
  zipped_file_name: 'stats.zip'
};

function clean() {
  log('Removing the old files in the directory')
  return del('prod-build/**', { force: true });
}

function createProdBuildFolder() {

  const dir = paths.prod_build;
  log(`Creating the folder if not exist  ${dir}`)
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    log('📁  Folder created:', dir);
  }

  return Promise.resolve('the value is ignored');
}

function buildAngularCodeTask(cb) {
  log('Building Angular code into the directory')
  return exec(`cd ${process.env.ANGULAR_FOLDER_NAME} && npm run build`, function (err, stdout, stderr) {
    log(stdout);
    log(stderr);
    cb(err);
  })
}

function copyAngularCodeTask() {
  log('Copying Angular code into the directory')
  return src(`${paths.angular_src}`)
    .pipe(dest(`${paths.angular_dist}`));
}

function copyNodeJSCodeTask() {
  log('Building and copying server code into the directory')
  return webpack_stream(webpack_config)
    .pipe(dest(`${paths.prod_build}`))
}

function zippingTask() {
  log('Zipping the code ')
  return src(`${paths.prod_build}/**`)
    .pipe(zip(`${paths.zipped_file_name}`))
    .pipe(dest(`${paths.prod_build}`))
}

exports.default = series(
  clean,
  createProdBuildFolder,
  buildAngularCodeTask,
  parallel(copyAngularCodeTask, copyNodeJSCodeTask),
  zippingTask
);
