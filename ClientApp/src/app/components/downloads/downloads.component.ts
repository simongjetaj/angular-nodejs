import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Inject,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { DownloadsService } from 'src/app/services/downloads.service';
import { IDownload } from 'src/app/models/downloads.interface';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { filter } from 'rxjs/operators';
import { DownloadsTimePeriodEnum } from 'src/app/enums/downloads.enum';

declare var google;

@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DownloadsComponent implements OnInit {
  @ViewChild('map') map: ElementRef;
  downloads: IDownload[] = [];
  socket: SocketIOClient.Socket;

  constructor(
    @Inject(DownloadsService) private downloadsService: DownloadsService,
    @Inject(ChangeDetectorRef) private cd: ChangeDetectorRef
  ) {
    this.socket = io.connect(environment.downloadsApi);
  }

  ngOnInit(): void {
    google.charts.load('current', {
      packages: ['geochart'],
      mapsApiKey: environment.mapsApiKey,
    });

    this.fetchDownloadsAndDrawMap();

    this.socket.on('downloadReceived', (download) => {
      this.cd.markForCheck();
      this.downloads.push(download.new);
      this.drawRegionsMap(this.downloads);
    });
  }

  onTimePeriodChange(timePeriod: string): void {
    this.fetchDownloadsAndDrawMap(timePeriod);
  }

  get DownloadsTimePeriod() {
    return DownloadsTimePeriodEnum;
  }

  private fetchDownloadsAndDrawMap(timePeriod?: string): void {
    this.downloadsService
      .getDownloads(timePeriod)
      .pipe(filter((downloads: IDownload[]) => downloads?.length >= 0))
      .subscribe((downloads: IDownload[]) => {
        this.cd.markForCheck();
        this.downloads = downloads;

        google.charts.setOnLoadCallback(() => {
          this.drawRegionsMap(this.downloads);
        });
      });
  }

  private drawRegionsMap(downloads: IDownload[]): void {
    const results = this.getResults(downloads);

    const data = google.visualization.arrayToDataTable([
      ['Country', 'Downloads'],
      ...results,
    ]);

    const chart = new google.visualization.GeoChart(this.map.nativeElement);

    chart.draw(data, {});
  }

  private getResults(downloads: IDownload[]) {
    const counts = downloads.reduce((accumulator, current) => {
      const name = current.name;
      accumulator[name] = ++accumulator[name] || 1;
      return accumulator;
    }, {});

    const results = [];

    for (const key of Object.keys(counts)) {
      results.push([key, counts[key]]);
    }

    return results;
  }
}
